#include "serialwidget.h"
#include "serialform.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SerialForm w;
    w.show();

    return a.exec();
}
