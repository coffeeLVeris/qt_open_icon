#ifndef DESKTOPWRAPPER_H
#define DESKTOPWRAPPER_H

#include <QWidget>

class DesktopWrapper
{
public:
    /*!
     * Get available geometry by given screen index.
     */
    static QRect availableGeometry(int index = 0);
    /*!
     * Get screen geometry by given screen index.
     */
    static QRect screenGeometry(int index = 0);
    /*!
     * Get all screen geometry.
     */
    static QRect geometry();

    /*!
     * Grab widget geometry pixmap by given rect.
     */
    static QPixmap grabWidget(QWidget *widget, const QRect &rect);
    /*!
     * Grab all screen geometry pixmap by given rect.
     */
    static QPixmap grabWindow(int x = 0, int y = 0, int w = -1, int h = -1);

};

#endif
