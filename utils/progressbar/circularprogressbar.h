/**
 ** @author:	 Greedysky
 ** @date:       2019.4.17
 ** @brief:      进度条组件
 */
#ifndef CIRCULARPROGRESSBAR_H
#define CIRCULARPROGRESSBAR_H

#include <QProgressBar>

class CircularProgressBar;

class CircularProgressDelegate : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qreal dashOffset WRITE setDashOffset READ dashOffset)
    Q_PROPERTY(qreal dashLength WRITE setDashLength READ dashLength)
    Q_PROPERTY(int angle WRITE setAngle READ angle)
public:
    explicit CircularProgressDelegate(CircularProgressBar *parent = 0);

    void setDashOffset(qreal offset);
    inline qreal dashOffset() const { return m_dashOffset; }

    void setDashLength(qreal length);
    inline qreal dashLength() const { return m_dashLength; }

    void setAngle(int angle);
    inline int angle() const { return m_angle; }

private:
    CircularProgressBar *m_progress;
    qreal m_dashOffset, m_dashLength;
    int m_angle;

};


class CircularProgressBar : public QProgressBar
{
    Q_OBJECT
    Q_PROPERTY(qreal lineWidth WRITE setLineWidth READ lineWidth)
    Q_PROPERTY(qreal size WRITE setSize READ size)
    Q_PROPERTY(QColor color WRITE setColor READ color)
public:
    explicit CircularProgressBar(QWidget *parent = nullptr);
    ~CircularProgressBar();

    void setLineWidth(qreal width);
    qreal lineWidth() const;

    void setSize(int size);
    int size() const;

    void setColor(const QColor &color);
    QColor color() const;

    virtual QSize sizeHint() const override;

protected:
    virtual void paintEvent(QPaintEvent *event) override;

    CircularProgressDelegate *m_delegate;
    QColor m_color;
    qreal m_penWidth;
    int m_size;

};

#endif
