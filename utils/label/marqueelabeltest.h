/**
 ** @author:	 Greedysky
 ** @date:       2019.4.15
 ** @brief:      跑马灯
 */
#ifndef MarqueeLabelTest_H
#define MarqueeLabelTest_H

#include <QMainWindow>

namespace Ui {
class MarqueeLabelTest;
}

class MarqueeLabelTest : public QMainWindow
{
    Q_OBJECT
public:
    explicit MarqueeLabelTest(QWidget *parent = nullptr);
    ~MarqueeLabelTest();

private:
    Ui::MarqueeLabelTest *ui;

};

#endif // MarqueeLabelTest_H
